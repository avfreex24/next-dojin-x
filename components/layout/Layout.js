import Navbar from './Navbar';
import Sidebar from './Sidebar';
export default function Layout({ children, mobile }) {
	return (
		<>
			<Navbar />
			<div className="min-h-screen bg-[#1E1E1E] flex justify-center">
				<div className="w-full">{children}</div>
			</div>
		</>
	);
}
