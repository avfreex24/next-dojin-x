import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { FiHome, FiSearch, FiBookmark, FiThumbsUp } from 'react-icons/fi';
import Image from 'next/image';
import { IconContext } from 'react-icons';
import Link from 'next/link';

export default function Navbar() {
	let router = useRouter();
	const [currentRoute, setCurrentRoute] = useState();
	const [width, setWidth] = useState();

	function handleWindowSizeChange() {
		setWidth(window.innerWidth);
	}

	useEffect(() => {
		const getRoute = () => {
			let route = router.asPath;
			if (route !== '/') {
				setCurrentRoute(route.split('/')[1]);
			} else {
				setCurrentRoute('home');
			}
		};
		getRoute();
	}, [router]);

	const mobileView = (
		<div className="w-full flex justify-center fixed inset-x-0 bottom-0 bg-[#1E1E1E] z-10">
			<div className="flex w-[80%] justify-between p-2">
				<Link
					className={`${
						currentRoute == 'home' ? 'text-[#92EBFF]' : 'text-white'
					}`}
					href="/"
					passHref
				>
					<a>
						<Image
							src="/logo-dojin.png"
							alt="logo-dojin"
							width="80"
							height="30"
							layout="fixed"
							priority
						/>
					</a>
				</Link>
				<Link
					className={`${
						currentRoute == 'home' ? 'text-[#92EBFF]' : 'text-white'
					}`}
					href="/sexy"
					passHref
				>
					<a>
						<Image
							src="/sexyEM.png"
							alt="logo-sexyEM"
							width="100"
							height="40"
							layout="fixed"
							priority
						/>
					</a>
				</Link>
				
				{/* <button
					className={`${
						currentRoute == 'search'
							? 'text-[#92EBFF]'
							: 'text-white'
					}`}
					onClick={() => router.push('/search')}
				>
					<IconContext.Provider value={{ size: 25 }}>
						<FiSearch />
					</IconContext.Provider>
				</button>

				<button
					className={`${
						currentRoute == 'bookmark'
							? 'text-[#92EBFF]'
							: 'text-white'
					}`}
					onClick={() => router.push('/sexy')}
				>
					<IconContext.Provider value={{ size: 25 }}>
						<FiThumbsUp />
					</IconContext.Provider>
				</button> */}
			</div>
		</div>
	);

	const desktopView = (
		<div className="navbar bg-base-100 grid justify-items-center">
			<Link
				className={`${
					currentRoute == 'home' ? 'text-[#92EBFF]' : 'text-white'
				}`}
				href="/"
				passHref
			>
				<a className="btn btn-ghost normal-case text-xl">
					<Image
						src="/logo-dojin.png"
						alt="logo-dojin"
						width="100"
						height="40"
						layout="fixed"
						priority
					/>
				</a>
			</Link>

			{/* <div className="navbar-end">
				<a className="btn">Get started</a>
			</div> */}
		</div>
		// <div className='fixed inset-x-0 top-0 z-10 p-3 bg-[#181818]'>
		//     <div className='flex justify-between px-10'>
		//         <div>
		//         <Link className={`${currentRoute == 'home' ? 'text-[#92EBFF]' : 'text-white'}`} href="/" passHref>
		//             <a><Image src="/logo-dojin.png" alt="logo-dojin" width="100" height="40" layout="fixed" priority /></a>
		//         </Link>
		//         </div>
		//         <div className='flex justify-between self-center space-x-7'>
		//         <IconContext.Provider value={{ size: 25 }} >
		//                 <FiThumbsUp className="text-white" />
		//             </IconContext.Provider>
		//             <button className='text-red-500 font-light'
		//                 onClick={() => router.push('/dojin')}
		//             >
		//             อ่านโดจิน แปลไทย
		//             </button>
		//             <button className='text-white font-light'
		//                 onClick={() => router.push('/search')}
		//             >
		//                 ค้นหา
		//             </button>
		//             {/* <button className='text-white font-light'
		//                 onClick={() => router.push('/bookmark')}
		//             >
		//                 Bookmark
		//             </button> */}
		//         </div>
		//     </div>
		// </div>
	);

	useEffect(() => {
		window.addEventListener('resize', handleWindowSizeChange);
		return () => {
			window.removeEventListener('resize', handleWindowSizeChange);
		};
	}, []);

	useEffect(() => {
		setWidth(window.innerWidth);
	}, []);

	return <>{width <= 768 ? <>{mobileView}</> : <>{desktopView}</>}</>;
	// }
}
