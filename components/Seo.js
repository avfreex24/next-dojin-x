import Head from 'next/head';
import { useRouter } from 'next/router';
import { NextSeo } from 'next-seo';
import { BreadcrumbJsonLd } from 'next-seo';
export const Seo = (props) => {
	const router = useRouter();
	const canonicalUrl = (router.asPath === '/' ? '' : router.asPath).split(
		'?'
	)[0];
	console.log(canonicalUrl);
	const current = new Date();
	return (
		<Head>
			<NextSeo
				title={`DOUJINแปลไทย ${props.title}`}
				description={`DOUJINแปลไทย การ์ตูนโดจินออนไลน์ ${props.desc}`}
				keywords={`DOUJIN ,DOJIN ,แปลไทย ,การ์ตูนโดจิน`}
				openGraph={{
					type: 'website',
					url: canonicalUrl,
					title: props.title,
					description: props.desc,
					images: [
						{
							url: props.cover,
							width: 800,
							height: 600,
							alt: props.title
						}
					]
				}}
			/>
		</Head>
	);
};
