import { useRouter } from 'next/router';
import { BookmarkManager } from '../../lib/store';
import { useEffect, useState } from 'react';
import Bookmark from './Bookmark';
import Link from 'next/link';

Array.prototype.randomSite = function () {
	return this[Math.floor(Math.random() * this.length)];
};

var colorXcard = [
	'#FDCB68',
	'#ea52b0',
	'#af7dd1',
	'#64CCF2',
	'#3FCFAB',
	'#FDCB68',
	'#E3455C'
];
let barcolor = colorXcard.randomSite();

export default function MangaXCard({ value }) {
	let router = useRouter();
	//const [isBookmark, setIsBookmark] = useState(false)
	//const [bookmarkData, setBookmarkValue] = useState()

	// const handleBookmark = (e) => {
	//     e.stopPropagation()
	//     if (isBookmark) {
	//         BookmarkManager.removeBookmark(value.id)
	//         setIsBookmark(false)
	//         if (!source) {
	//             router.reload()
	//         }
	//     } else {
	//         BookmarkManager.addBookmark(source, value.id, value.title, value.cover)
	//         setIsBookmark(true)
	//     }
	// }

	// useEffect(() => {
	//     if (bookmarkMode && isBookmark) {
	//         setBookmarkValue(BookmarkManager.getSingleBookmarkId(value.id))
	//     }
	// }, [bookmarkMode, isBookmark])

	// useEffect(() => {
	//     setIsBookmark(BookmarkManager.checkBookmark(value.id))
	// }, [isBookmark, value.id])

	return (
		<div
			className={`flex flex-col space-y-2 cursor-pointer glass `}
			// onClick={() => {
			//     if (isBookmark && bookmarkMode && bookmarkData.chapter) {
			//         router.push(`/r/${bookmarkData.source}/${bookmarkData.id}/${bookmarkData.chapter}`)
			//     } else {
			//          router.push(`/m/${source ? source : value.source}/${value.id}`)
			//     }
			//  }}
		>
			<div className="relative h-56 sm:h-72">
				<div className="h-56 sm:h-72 absolute inset-0 rounded-xl">
					<div
						className="absolute right-0 bottom-2 px-5 py-3"
						//onClick={(e) => handleBookmark(e)}
					></div>
				</div>
				<Link
					href={`/sexy/${value?.links?.replace(/\//g, '')}`}
					passHref
				>
					<a>
						<img
							src={value.src}
							className="rounded-xl h-56 sm:h-72 w-full"
							alt={`${value.title} Sexy Dojin`}
						/>
					</a>
				</Link>
			</div>
			<div className="flex flex-col space-y-1">
				<Link
					href={`/sexy/${value?.links?.replace(/\//g, '')}`}
					passHref
				>
					<a>
						<p className="text-white text-xs capitalize line-clamp-2 font-semibold">
							{value.title}
						</p>
					</a>
				</Link>
				<p className="text-white opacity-80 text-xs font-thin sm:font-normal truncate">
					Tags: {value?.tags}
				</p>
			</div>
		</div>
	);
}
