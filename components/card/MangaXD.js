import { useRouter } from 'next/router';
import { BookmarkManager } from '../../lib/store';
import { useEffect, useState } from 'react';
import Bookmark from './Bookmark';
import Link from 'next/link';

Array.prototype.randomSite = function () {
	return this[Math.floor(Math.random() * this.length)];
};

var colorXcard = [
	'#FDCB68',
	'#ea52b0',
	'#af7dd1',
	'#64CCF2',
	'#3FCFAB',
	'#FDCB68',
	'#E3455C'
];
let barcolor = colorXcard.randomSite();

export default function MangaXD({ value }) {
	let router = useRouter();
	//const [isBookmark, setIsBookmark] = useState(false)
	//const [bookmarkData, setBookmarkValue] = useState()

	// const handleBookmark = (e) => {
	//     e.stopPropagation()
	//     if (isBookmark) {
	//         BookmarkManager.removeBookmark(value.id)
	//         setIsBookmark(false)
	//         if (!source) {
	//             router.reload()
	//         }
	//     } else {
	//         BookmarkManager.addBookmark(source, value.id, value.title, value.cover)
	//         setIsBookmark(true)
	//     }
	// }

	// useEffect(() => {
	//     if (bookmarkMode && isBookmark) {
	//         setBookmarkValue(BookmarkManager.getSingleBookmarkId(value.id))
	//     }
	// }, [bookmarkMode, isBookmark])

	// useEffect(() => {
	//     setIsBookmark(BookmarkManager.checkBookmark(value.id))
	// }, [isBookmark, value.id])

	return (
		<div className="relative w-full h-auto">
			<img
				src={value.src}
				className="rounded-xl"
				alt={`${value.title} Sexy Dojin`}
			/>
		</div>
	);
}
