import { useRouter } from 'next/router';
import { BookmarkManager } from '../../lib/store';
import { useEffect, useState } from 'react';
import Bookmark from './Bookmark';
import Link from 'next/link';
import { IconContext } from 'react-icons';
import { FiThumbsUp } from 'react-icons/fi';
import Image from 'next/image';
Array.prototype.randomSite = function () {
	return this[Math.floor(Math.random() * this.length)];
};

var colorXcard = [
	'#FDCB68',
	'#ea52b0',
	'#af7dd1',
	'#64CCF2',
	'#3FCFAB',
	'#FDCB68',
	'#E3455C'
];
let barcolor = colorXcard.randomSite();

export default function MangaDojin({ valueX, xn, ntag }) {
	let router = useRouter();
	//const [isBookmark, setIsBookmark] = useState(false)
	//const [bookmarkData, setBookmarkValue] = useState()

	// const handleBookmark = (e) => {
	//     e.stopPropagation()
	//     if (isBookmark) {
	//         BookmarkManager.removeBookmark(value.id)
	//         setIsBookmark(false)
	//         if (!source) {
	//             router.reload()
	//         }
	//     } else {
	//         BookmarkManager.addBookmark(source, value.id, value.title, value.cover)
	//         setIsBookmark(true)
	//     }
	// }

	// useEffect(() => {
	//     if (bookmarkMode && isBookmark) {
	//         setBookmarkValue(BookmarkManager.getSingleBookmarkId(value.id))
	//     }
	// }, [bookmarkMode, isBookmark])

	// useEffect(() => {
	//     setIsBookmark(BookmarkManager.checkBookmark(value.id))
	// }, [isBookmark, value.id])

	return (
		<div
			className={`flex flex-col space-y-2 cursor-pointer glass `}
			// onClick={() => {
			//     if (isBookmark && bookmarkMode && bookmarkData.chapter) {
			//         router.push(`/r/${bookmarkData.source}/${bookmarkData.id}/${bookmarkData.chapter}`)
			//     } else {
			//          router.push(`/m/${source ? source : value.source}/${value.id}`)
			//     }
			//  }}<div className='h-56 sm:h-72 absolute inset-0 rounded-xl'>
			//<div className='absolute right-0 bottom-2 px-5 py-3'
			// onClick={(e) => handleBookmark(e)}
			// ></div>
			// </div>
		>
			<div className="relative">
				<Link href={`/dojin/${ntag + '-' + xn}`} passHref>
					<a>
						<Image
							src={valueX.prev_url}
							width={100}
							height={200}
							layout="responsive"
							className="rounded-xl h-56 sm:h-72 w-full"
							priority
							alt={`${valueX.title} Sexy Dojin`}
						/>
					</a>
				</Link>
			</div>
			<div className="flex flex-col space-y-1">
				<Link href={`/dojin/${ntag + '-' + xn}`} passHref>
					<a>
						<h2 className="text-center px-2 py-2 text-md md:text-xl ">
							{valueX?.title
								?.replace(/\d.+เเล้ว/g, '')
								.replace(/ดู.+ครั้ง/g, '')}
						</h2>
					</a>
				</Link>
				<p className="text-center text-white opacity-80 text-xs py-4 px-4 font-thin sm:font-normal truncate">
					โดจิน {valueX?.badge}
				</p>
			</div>
		</div>
	);
}
