import { useRouter } from 'next/router';
import { BookmarkManager } from '../../lib/store';
import { useEffect, useState } from 'react';
import Bookmark from './Bookmark';
import Link from 'next/link';
import { IconContext } from 'react-icons';
import { FiThumbsUp } from 'react-icons/fi';
import Image from 'next/image';
Array.prototype.randomSite = function () {
	return this[Math.floor(Math.random() * this.length)];
};

var colorXcard = [
	'#FDCB68',
	'#ea52b0',
	'#af7dd1',
	'#64CCF2',
	'#3FCFAB',
	'#FDCB68',
	'#E3455C'
];
let barcolor = colorXcard.randomSite();

export default function MangaDojinDetil({ valueXD }) {
	let router = useRouter();
	//const [isBookmark, setIsBookmark] = useState(false)
	//const [bookmarkData, setBookmarkValue] = useState()

	// const handleBookmark = (e) => {
	//     e.stopPropagation()
	//     if (isBookmark) {
	//         BookmarkManager.removeBookmark(value.id)
	//         setIsBookmark(false)
	//         if (!source) {
	//             router.reload()
	//         }
	//     } else {
	//         BookmarkManager.addBookmark(source, value.id, value.title, value.cover)
	//         setIsBookmark(true)
	//     }
	// }

	// useEffect(() => {
	//     if (bookmarkMode && isBookmark) {
	//         setBookmarkValue(BookmarkManager.getSingleBookmarkId(value.id))
	//     }
	// }, [bookmarkMode, isBookmark])

	// useEffect(() => {
	//     setIsBookmark(BookmarkManager.checkBookmark(value.id))
	// }, [isBookmark, value.id])

	return (
		<>
			<div className="w-full h-auto mx-auto px-4 gap-4">
				{valueXD['img_pack'].map((value, index) => (
					<Image
						key={index}
						src={value.photo_url}
						width={100}
						height={200}
						layout="responsive"
						priority
						alt={`${value.title} โดจินแปลไทย`}
					/>
				))}
				<div className="py-4">X</div>
			</div>
		</>
	);
}
