import Link from 'next/link';

Array.prototype.randomSite = function () {
	return this[Math.floor(Math.random() * this.length)];
};

let colorXcard = [
	'border-yellow-500',
	'border-white',
	'border-slate-500',
	'border-gray-500',
	'border-zinc-500',
	'border-red-500',
	'border-amber-500',
	'border-lime-500',
	'border-green-500',
	'border-emerald-500',
	'border-teal-500',
	'border-cyan-500',
	'border-sky-500',
	'border-blue-500',
	'border-indigo-500',
	'border-violet-500',
	'border-purple-500',
	'border-fuchsia-500',
	'border-pink-500',
	'border-rose-500',
	'border-orange-500'
];
let colorXtext = [
	'text-yellow-500',
	'text-white',
	'text-slate-500',
	'text-gray-500',
	'text-zinc-500',
	'text-red-500',
	'text-amber-500',
	'text-lime-500',
	'text-green-500',
	'text-emerald-500',
	'text-teal-500',
	'text-cyan-500',
	'text-sky-500',
	'text-blue-500',
	'text-indigo-500',
	'text-violet-500',
	'text-purple-500',
	'text-fuchsia-500',
	'text-pink-500',
	'text-rose-500',
	'text-orange-500'
];
let barcolor = colorXcard.randomSite();

export default function TagsList(props) {
	return (
		<>
			<div
				className={`bg-gray-500/10 border-2 h-14 ${colorXcard.randomSite()} rounded-lg hover:scale-110 `}
			>
				<Link href="/[tagsx]" as={`/${props.vx}`}>
					<a title={`โดจินแปลไทย ประเภท ${props.vx}`}>
						<p className="text-center px-2 break-all">
							<span className="text-xs">
								<b className={`${colorXtext.randomSite()}`}>
									โดจินแปลไทย{props.vx.slice(0, 1)}
								</b>{' '}
								ประเภท
							</span>
							<br />
							<em
								className={`${colorXtext.randomSite()} uppercase`}
							>
								{props.vx}
							</em>
						</p>
					</a>
				</Link>
			</div>
		</>
	);
}
