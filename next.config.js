// const withPWA = require('next-pwa');

// module.exports = withPWA({
// 	pwa: {
// 		dest: 'public',
// 		register: true,
// 		skipWaiting: true
// 	}
// });

/** @type {import('next').NextConfig} */
const runtimeCaching = require('next-pwa/cache');
const withPWA = require('next-pwa')({
	dest: 'public',
	register: true,
	skipWaiting: true,
	runtimeCaching,
	buildExcludes: [/middleware-manifest.json$/],
	disable: process.env.NODE_ENV === 'development'
});

const nextConfig = withPWA({
	reactStrictMode: true,
	swcMinify: true,
	images: {
		domains: ['ilovedoujin.com']
	},
	minimumCacheTTL: 60,
	trailingSlash: true,

});
module.exports = nextConfig;
