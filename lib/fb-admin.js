import { initializeApp } from 'firebase/app';
import { getDatabase } from 'firebase/database';

const firebaseConfig = {
	apiKey: 'AIzaSyCM2s4hW08qJWdnp5srd0sNnGfl-3b3sr0',
	authDomain: 'dump-all.firebaseapp.com',
	databaseURL: 'https://dump-all-default-rtdb.firebaseio.com',
	projectId: 'dump-all',
	storageBucket: 'dump-all.appspot.com',
	messagingSenderId: '277473544018',
	appId: '1:277473544018:web:00cb3eea594190de5b5c8b',
	measurementId: 'G-K7NVEP8Y3B'
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const Database = app;

export default Database;
