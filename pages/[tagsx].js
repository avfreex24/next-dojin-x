import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import Layout from '../components/layout/Layout';
import { Seo } from '../components/Seo';
import Image from 'next/image';
import { NextSeo } from 'next-seo';
import { useRouter } from 'next/router';
import { WebPageJsonLd } from 'next-seo';
import { ArticleJsonLd } from 'next-seo';
import { NewsArticleJsonLd } from 'next-seo';
import MangaDojin from '../components/card/MangaDojin';

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min) + min);
}
export async function getServerSideProps({ query }) {
	// Fetch data from external API
	const { tagsx } = query;
	//const res = await fetch(`https://ufax24board-default-rtdb.firebaseio.com/pic/${ind}.json`);
	const res = await fetch(
		`https://dump-all-default-rtdb.firebaseio.com/${tagsx}.json`
	);
	const data = await res.json();

	//const data = `https://doball24.com/player/live.php?ch=${index}`
	// console.log(data);

	return {
		props: { postXsball: data, ttag: tagsx }
	};
}

export default function Balllive({ postXsball, ttag }) {
	//console.log(postXsball)
	const router = useRouter();
	const [postNum, setPostNum] = useState(31);
	function handleClick() {
		setPostNum((prevPostNum) => prevPostNum + 15);
	}
	const canonicalUrl = (router.asPath === '/' ? '' : router.asPath).split(
		'?'
	)[0];
	const current = new Date();
	const date = `${current.getDate()}/${
		current.getMonth() + 1
	}/${current.getFullYear()}`;
	return (
		<>
			<Head>
				<title>
					{`โดจิน แปลไทย ประเภท ${ttag} อ่านโดจิน มังงะ dojin แปลไทย การ์ตูน18+ โดจินออนไลน์`}
				</title>
				<meta
					name="description"
					content={`อ่านโดจินฟรี2023 ประเภท ${ttag} แปลdojinภาษาไทย แปลไทยโดยคนไทย ส่งเสริมการ์ตูน18+ กลุ่มโดจินออนไลน์`}
				/>
				<NextSeo
					title={`DOUJINแปลไทย ${ttag}`}
					description={`อ่านโดจินฟรี2023 ประเภท ${ttag} แปลdojinภาษาไทย แปลไทยโดยคนไทย ส่งเสริมการ์ตูน18+ กลุ่มโดจินออนไลน์`}
					keywords={`DOUJIN ,DOJIN ,แปลไทย ,การ์ตูนโดจิน`}
					openGraph={{
						type: 'website',
						url: canonicalUrl,
						title: `DOUJINแปลไทย ${ttag}`,
						description: `อ่านโดจินฟรี2023 ประเภท ${ttag} แปลdojinภาษาไทย แปลไทยโดยคนไทย ส่งเสริมการ์ตูน18+ กลุ่มโดจินออนไลน์`,
						images: [
							{
								url: '/Ibaraki_01.png',
								width: 800,
								height: 600,
								alt: `DOUJINแปลไทย ${ttag}`
							}
						]
					}}
				/>
			</Head>
			<Layout>
				<h2 className="text-lg text-center text-orange-500 px-4 py-4">
					โดจินแปลไทย เสียวน้ำเดินทุกเรื่อง
				</h2>
				<div className="grid grid-cols-2 gap-4 px-4 py-4 md:grid-cols-5">
					{postXsball.slice(1, postNum).map((post, index) => (
						<MangaDojin
							key={index}
							valueX={post}
							xn={index}
							ntag={ttag}
						/>
					))}
				</div>
				<div className="grid grid-cols-1 justify-items-center ">
					<button
						className="xbutton mb-28 md:w-1/2"
						onClick={handleClick}
					>
						<span></span>
						<span></span>
						<span></span>
						<span></span>โดจินแปลไทย
					</button>
				</div>
			</Layout>
			{/* <Main /> */}
		</>
	);
}

// function loadJson(url) {
//     return fetch(url)
//       .then(response => {
//         if (response.status == 200) {
//           return response.json();
//         } else {
//           throw new Error(response.status);
//         }
//       });
//   }

// function getNewsInfo() {
//     return axios
//         .get(`https://h-ani.com/doujin-thai`)
//         .then(function ({ data }) {
//             let $ = cheerio.load(data);

//             const pattern = /s='(?<img>[^']+)';\w+\s\w+=\['(?<id>\w+_\d+)'];/gm;
//             const images = [...data.matchAll(pattern)].map(({ groups }) => ({ id: groups.id, img: groups.img.replace('\\x3d', '') }))

//             const allNewsInfo = Array.from($('.WlydOe')).map((el) => {
//                 return {
//                     link: $(el).attr('href'),
//                     source: $(el).find('.CEMjEf span').text().trim(),
//                     title: $(el).find('.mCBkyc').text().trim().replace('\n', ''),
//                     snippet: $(el).find('.GI74Re').text().trim().replace('\n', ''),
//                     image: images.find(({ id, img }) => id === $(el).find('.uhHOwf img').attr('id'))?.img || "No image",
//                     date: $(el).find('.ZE0LJd span').text().trim(),
//                 }
//             });
//             console.log(allNewsInfo)
//             return allNewsInfo;
//         });
// }

//var keydata = getRandomInt(0, 380);
// export const getStaticProps = async ({query}) => {
//     //let $ = cheerio.load(data);
//     const res = await fetch(`https://dump-all-default-rtdb.firebaseio.com/${query.tagsx}.json`);
//     //getNewsInfo().catch((asss) => {
//     //    console.log(asss)
//     //});
//     //getNewsInfo().then(alert);
//     const data = await res.json();
//     // for (const key in data) {
//     //   const imageMedia = await fetch(
//     //     `https://lucaclub88.net/wp-json/wp/v2/media/${data[key].featured_media}`
//     //   );
//     //   const imageMediaData = await imageMedia.json();
//     //   data[key].image = imageMediaData.source_url;
//     // }

//     //const nuxm = getRandomInt(0, 6000);
//     //console.log(result);
//     return {
//         props: {
//          postXsball: data,
//         },
//         revalidate: 10,
//     };
// };
