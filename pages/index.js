import { useRef, useEffect, useState } from 'react';
import Image from 'next/image';
import { Seo } from '../components/Seo';
import Layout from '../components/layout/Layout';
import { getDatabase, ref, child, get, onValue } from 'firebase/database';
import Database from '../lib/fb-admin';
import TagsList from '../components/card/TagsList';
import { execOnce } from 'next/dist/shared/lib/utils';
import Head from 'next/head';
const dbRef = ref(getDatabase(Database));

const listTag = [
	'netorare',
	'Loli',
	'school',
	'Mother',
	'Color',
	'shota',
	'Incest',
	'Sister',
	'Group',
	'rape',
	'Toy',
	'Virgin',
	'skin',
	'Glass',
	'Teacher',
	'maid',
	'yuri',
	'manhwa'
];

export default function Home() {
	// const [Tags, setTags] = useState([]);

	// useEffect(() => {
	//   const fetchData = async () => {
	//     get(child(dbRef, `/`)).then((snapshot) => {
	//       const childKeyX = [];
	//       if (snapshot.exists()) {
	//         snapshot.forEach((childSnapshot) => {
	//           const childKey = childSnapshot.key;
	//           childKeyX.push(childKey)
	//           //const childData = childSnapshot.val();
	//           //console.log(childKey)
	//         });
	//         setTags(childKeyX)
	//       } else {
	//         console.log("No data available");
	//       }
	//     }).catch((error) => {
	//       console.error(error);
	//     });
	//   };

	//   fetchData();
	// }, []);

	// get(child(dbRef, `/`), (snapshot) => {
	//   const childKeyX = [];
	//   snapshot.forEach((childSnapshot) => {
	//     const childKey = childSnapshot.key;
	//     childKeyX.push(childKey)
	//     //const childData = childSnapshot.val();
	//     //console.log(childKey)
	//   });
	//   setTags(childKeyX)
	// }, {
	//   onlyOnce: true
	// });

	return (
		<div>
			<Head>
				<title>DOUJINแปลไทย การ์ตูนโดจินออนไลน์</title>
				<meta
					name="description"
					content={`อ่านโดจินฟรี2023 การ์ตูนโดจินออนไลน์ แปลdojinภาษาไทย แปลไทยโดยคนไทย ส่งเสริมการ์ตูน18+ กลุ่มโดจินออนไลน์`}
				/>
			</Head>
			<Seo
				cover={'/wLav24.jpg'}
				title={
					'โดจิน อ่านโดจิน มังงะ dojin แปลไทย การ์ตูน18+ โดจินออนไลน์'
				}
				desc={
					'อ่านโดจินฟรี2023 แปลdojinภาษาไทย แปลไทยโดยคนไทย ส่งเสริมการ์ตูน18+ กลุ่มโดจินออนไลน์'
				}
			/>
			<Layout>
				<h1 className="text-center py-4 px-4 text-xl md:text-3xl">
					<b>DOUJINแปลไทย</b>{' '}
					<span className="text-violet-400">อ่านโดจินภาพสี</span>
				</h1>
				<p className="text-center text-xs py-4 px-4">
					โดจินแปลไทย ภาพชัด โดจิน คือ
					ผลงานสร้างสรรค์ที่เกิดจากความชอบสิ่งใดสิ่งหนึ่ง เมืองไทย
					เรียกหนังสือทำมือ จะทำเดี่ยวหรือทำกลุ่มก็ได้
					แต่ส่วนใหญ่ที่กระจายมาถึงบ้านเราเป็นส่วนของ นิจิโซซาคุ
					(二次創作) หรือ ผลงานพาโรดี้ ล้อเลียนเรื่องที่ตัวเองชอบ
					ทำให้ ความหมายของโดจินค่อนข้างจะไปเป็น Fanmade
				</p>
				<div className="bg-[url(/DDG.png)] bg-no-repeat bg-auto ">
					<div className="w-full grid grid-cols-1 md:grid-cols-4 lg:grid-cols-6 gap-4 px-8 mt-4">
						{listTag.map((value, index) => (
							<TagsList key={index} vx={value} />
						))}
					</div>

					<div className="hero min-h-screen bg-[url(/ตัวการตูนโดจิน.png)] bg-no-repeat bg-auto mt-8">
						<div className="hero-overlay bg-opacity-60"></div>
						<div className="hero-content text-center text-neutral-content">
							<div className="">
								<div className="text-center py-4 px-4">
									<h2 className="text-2xl">
										โดจิน
										Doujin（同人）โดจินมาจากภาษาญี่ปุ่น
										โดจินแปลไทย มาจาก dojin.info
									</h2>
									<p className="text-xs md:text-md">
										โดจินชิ Doujinshi
										（同人誌）เป็นคำย่อของคำว่า 同人雑誌
										Doujjinzasshi (โดจินซัทชิ) แปลว่า
										นิตยสารของคนที่ชื่นชอบอะไรอย่างเดียวกัน
										ประมาณสมัยเมจิ โดยส่วนใหญ่เป็น บทกวี และ
										บทกลอน ที่เหล่านักกวี
										มารวมตัวกันเขียนหนังสือเวียน หรือ 回覧誌
										(ไครันชิ) ระหว่างในกลุ่ม
										จนกระทั่งหนังสือได้รับการตีพิมพ์
									</p>
								</div>

								<div className="text-center py-4 px-4">
									<h3 className="text-2xl">
										ยุคถัดมาของการ์ตูนโดจิน
									</h3>
									<p className="text-xs md:text-md">
										ยุคถัดมาของการ์ตูน
										กลุ่มของเท็ตสึกะโอซามุ
										เขียนการ์ตูนเวียนส่งให้กันอ่านทั่วประเทศ
										และนักเขียนทั้งหลายเริ่มส่งไปลงตีพิมพ์ในนิตยสารชื่อ
										COM จนกระทั่งนิตยสารนี้ปิดตัว
										ทำให้เหล่านักเขียนการ์ตูนหวั่นใจ
										เป็นต้นกำเนิดของคอมิเกะ หรือ Comic
										Market ที่เรารู้จักกัน โดยเริ่มในปี 1975
										ในปัจจุบันการตีพิมพ์ถูกลง
										ขนส่งดีขึ้นทำให้มีคนทำเองเยอะ แต่คำว่า
										เซอร์เคิ่ล ที่ใช้เรียกกลุ่ม กับ ผลงาน
										โดจินชิ ยังคงอยู่เหมือน
									</p>
								</div>

								<div className="text-center py-4 px-4">
									<h4 className="text-2xl">
										ยุคถัดมาของการ์ตูนโดจิน
									</h4>
									<p className="text-xs md:text-md">
										ตั้งแต่เกิดกระแส Yaoi อันยิ่งใหญ่
										นั่นคือ สึบาสะ เซนต์เซย่า ซามูไรทรูเปอร์
										กันดั้มวิง และอื่นๆ ทำให้ผลงานโดจินสาย Y
										ครองคอมิเกะไปเกือบหมด
										และผู้หญิงครองพื้นที่ของเซอร์เคิ่ลไปถึง
										2 จาก 3 วัน โดยแบ่งเป็น วันแรก
										วันผู้หญิง วันที่สอง วันผู้หญิง
										วันที่สาม วันผู้ชาย สายเกมส์ อย่าง โทโฮ
										หรือ สายโมเอ๊
										กลับมาเรียกพื้นที่ให้เหล่าชายหนุ่มอีกครั้ง
										รวมถึง สายดนตรี และคอสเพลย์อีกด้วย
										สำหรับการจัดเซอร์เคิ่ลปีล่าสุดที่คอมิเกะแบ่งเป็น
										วันแรก วันผู้หญิง วันที่สอง วันผู้ชาย
										วันที่สาม วันเกมส์ ดิจิตอล
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</Layout>
		</div>
	);
}
