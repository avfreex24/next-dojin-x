import Head from 'next/head';
import Layout from '../../components/layout/Layout';
import Image from 'next/image';
import { NextSeo } from 'next-seo';
import { useRouter } from 'next/router';
import { WebPageJsonLd } from 'next-seo';
import { ArticleJsonLd } from 'next-seo';
import { NewsArticleJsonLd } from 'next-seo';
import Link from 'next/link';
import MangaDojinDetil from '../../components/card/MangaDojinDetil';

export async function getServerSideProps({ query }) {
	// Fetch data from external API
	const { ind } = query;
	const xints = parseInt(ind.split('-')[1]) + 1;
	const naTag = ind.split('-')[0];
	//const res = await fetch(`https://ufax24board-default-rtdb.firebaseio.com/pic/${ind}.json`);
	const res = await fetch(
		`https://dump-all-default-rtdb.firebaseio.com/${naTag}/${xints}.json`
	);
	const data = await res.json();

	//const data = `https://doball24.com/player/live.php?ch=${index}`
	//console.log(result);

	return {
		props: { BallXDetail: data }
	};
}
export default function DojinX({ BallXDetail }) {
	// console.log(BallXDetail);
	const router = useRouter();
	const canonicalUrl = (router.asPath === '/' ? '' : router.asPath).split(
		'?'
	)[0];
	const current = new Date();
	const date = `${current.getDate()}/${
		current.getMonth() + 1
	}/${current.getFullYear()}`;
	// const {id} = query
	//console.log(BallXDetail?.title.split("วันที่เเล้ว")[1])
	return (
		<>
			<Head>
				<title>
					{`โดจิน แปลไทย เรื่อง ${BallXDetail.title
						?.replace(/\d.+เเล้ว/g, '')
						.replace(
							/ดู.+ครั้ง/g,
							'dojin.info'
						)} อ่านโดจิน มังงะ dojin แปลไทย การ์ตูน18+ โดจินออนไลน์`}
				</title>
				<meta
					name="description"
					content={`อ่านโดจินฟรี2023 ${BallXDetail.title
						?.replace(/\d.+เเล้ว/g, '')
						.replace(
							/ดู.+ครั้ง/g,
							'dojin.info'
						)} แปลdojinภาษาไทย แปลไทยโดยคนไทย ส่งเสริมการ์ตูน18+ กลุ่มโดจินออนไลน์`}
				/>
				<NextSeo
					title={`DOUJINแปลไทย ${BallXDetail.title
						?.replace(/\d.+เเล้ว/g, '')
						.replace(/ดู.+ครั้ง/g, 'dojin.info')}`}
					description={`อ่านโดจินฟรี2023 ${BallXDetail.title
						?.replace(/\d.+เเล้ว/g, '')
						.replace(
							/ดู.+ครั้ง/g,
							'dojin.info'
						)} แปลdojinภาษาไทย แปลไทยโดยคนไทย ส่งเสริมการ์ตูน18+ กลุ่มโดจินออนไลน์`}
					keywords={`DOUJIN ,DOJIN ,แปลไทย ,การ์ตูนโดจิน`}
					openGraph={{
						type: 'website',
						url: canonicalUrl,
						title: BallXDetail.title
							?.replace(/\d.+เเล้ว/g, '')
							.replace(/ดู.+ครั้ง/g, 'dojin.info'),
						description: `อ่านโดจินฟรี2023 ${BallXDetail.title
							?.replace(/\d.+เเล้ว/g, '')
							.replace(
								/ดู.+ครั้ง/g,
								'dojin.info'
							)} แปลdojinภาษาไทย แปลไทยโดยคนไทย ส่งเสริมการ์ตูน18+ กลุ่มโดจินออนไลน์`,
						images: [
							{
								url: BallXDetail['img_pack'][0].photo_url,
								width: 800,
								height: 600,
								alt: BallXDetail.title
									?.replace(/\d.+เเล้ว/g, '')
									.replace(/ดู.+ครั้ง/g, 'dojin.info')
							}
						]
					}}
				/>
			</Head>
			<Layout>
				<>
					<h1 className="text-red-300 text-lg text-center py-8 px-4">
						โดจิน {BallXDetail.badge}{' '}
						{BallXDetail.title
							?.replace(/\d.+เเล้ว/g, '')
							.replace(/ดู.+ครั้ง/g, 'dojin.info')}
					</h1>
					<div className="grid grid-cols-1 gap-4">
						<MangaDojinDetil valueXD={BallXDetail} />
					</div>
				</>
			</Layout>
		</>
	);
}
