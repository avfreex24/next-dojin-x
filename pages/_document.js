import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
	render() {
		return (
			<Html lang="th">
				<Head>
					<meta charSet="utf-8" />
					<meta name="theme-color" content="#319197" />
					<meta
						name="apple-mobile-web-app-status-bar-style"
						content="black"
					/>
					<meta name="format-detection" content="telephone=no" />
					<meta name="lang" content="th" />
					<meta name="author" content="ChangYed" />
					<link rel="manifest" href="/manifest.json" />
					<link rel="icon" href="/favicon.ico" />
					<link rel="apple-touch-icon" href="/favicon.ico" />
					<link
						rel="apple-touch-icon"
						sizes="72x72"
						href="/logo_ctl.png"
					/>
					<link
						rel="apple-touch-icon"
						sizes="114x114"
						href="/logo_ctl.png"
					/>
					<link
						rel="apple-touch-icon"
						sizes="144x144"
						href="/logo_ctl.png"
					/>
				</Head>
				<body>
					<Main />
					<NextScript />
				</body>
			</Html>
		);
	}
}

export default MyDocument;
