// pages/_app.js
import '../styles/globals.css';
import 'tailwindcss/tailwind.css';
import { GoogleAnalytics } from 'nextjs-google-analytics';
//import { Seo } from '../components/Seo';

function MyApp({ Component, pageProps }) {
	<GoogleAnalytics strategy="lazyOnload" />;
	return <Component {...pageProps} />;
}

export default MyApp;
