import Head from 'next/head';
import Layout from '../../components/layout/Layout';
import Image from 'next/image';
import { NextSeo } from 'next-seo';
import { useRouter } from 'next/router';
import { WebPageJsonLd } from 'next-seo';
import { ArticleJsonLd } from 'next-seo';
import { NewsArticleJsonLd } from 'next-seo';
import Link from 'next/link';
import MangaXD from '../../components/card/MangaXD';

export async function getServerSideProps({ query }) {
	const { idx } = query;
	const res = await fetch(`https://api.ufabetlive.com/buond/${idx}`);
	const data = await res.json();
	//const result = data.filter((word) => word.id == index);
	//console.log(data)
	return {
		props: { BallLiveDetail: data['Buondua'] }
	};
}
export default function DooBallPlay({ BallLiveDetail }) {
	//console.log(BallLiveDetail);
	const router = useRouter();
	const canonicalUrl = (router.asPath === '/' ? '' : router.asPath).split(
		'?'
	)[0];
	const current = new Date();
	const date = `${current.getDate()}/${
		current.getMonth() + 1
	}/${current.getFullYear()}`;
	// const {id} = query
	//console.log(BallLiveDetail)
	return (
		<Layout>
			<div className="grid grid-cols-2 gap-2 px-4 py-4 glass md:grid-cols-5">
				{BallLiveDetail.map((BallCard) => {
					return <MangaXD key={BallCard.index} value={BallCard} />;
				})}
			</div>
		</Layout>
	);
}
