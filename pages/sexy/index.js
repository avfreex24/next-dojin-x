import Head from 'next/head';
import Layout from '../../components/layout/Layout';
import Image from 'next/image';
import { NextSeo } from 'next-seo';
import { useRouter } from 'next/router';
import { WebPageJsonLd } from 'next-seo';
import { ArticleJsonLd } from 'next-seo';
import { NewsArticleJsonLd } from 'next-seo';
import MangaXCard from '../../components/card/MangaXCard';

export default function Balllive({ postsball }) {
	const router = useRouter();
	const canonicalUrl = (router.asPath === '/' ? '' : router.asPath).split(
		'?'
	)[0];
	const current = new Date();
	const date = `${current.getDate()}/${
		current.getMonth() + 1
	}/${current.getFullYear()}`;
	return (
		<>
			<Layout>
				<h2 className="text-lg text-center text-orange-500 px-4 py-4">
					Refresh เพื่อเรียกน้องๆชุดใหม่
				</h2>
				<div className="grid grid-cols-2 gap-4 px-4 py-4 md:grid-cols-5">
					{postsball['Buondua'].map((BallCard) => {
						return (
							<MangaXCard key={BallCard.index} value={BallCard} />
						);
					})}
				</div>
			</Layout>
			{/* <Main /> */}
		</>
	);
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min) + min);
}

var keydata = getRandomInt(0, 300);
export const getStaticProps = async () => {
	const res = await fetch(`https://api.ufabetlive.com/buondua/${keydata}`);
	const data = await res.json();
	// for (const key in data) {
	//   const imageMedia = await fetch(
	//     `https://lucaclub88.net/wp-json/wp/v2/media/${data[key].featured_media}`
	//   );
	//   const imageMediaData = await imageMedia.json();
	//   data[key].image = imageMediaData.source_url;
	// }

	return {
		props: {
			postsball: data
		},
		revalidate: 10
	};
};
